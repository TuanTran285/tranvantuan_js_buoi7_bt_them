const submit = document.querySelector("#submit");

function inSoNguyenTo() {
  const n = document.querySelector("#nhap-so").value * 1;
  const reuslt = document.querySelector("#result");
  let content = "";
  for (let i = 0; i <= n; i++) {
    const soNguyenTo = kiemTraSpNguyenTo(i);
    if (soNguyenTo == true) {
      content += i + " ";
    }
  }
  reuslt.innerHTML = `=> ${content}`;
}

submit.addEventListener("click", function () {
  inSoNguyenTo();
});

function kiemTraSpNguyenTo(n) {
  let isFlag = true;
  if (n < 2) {
    isFlag = false;
  } else if (n == 2) {
    isFlag = true;
  } else if (n % 2 == 0) {
    isFlag = false;
  } else {
    for (let i = 3; i <= Math.sqrt(n); i += 2) {
      if (n % i == 0) {
        isFlag = false;
        return;
      }
    }
  }
  return isFlag;
}
